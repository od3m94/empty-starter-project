const { getEnvironmentVariable } = require('../src/shared/misc-helper')

module.exports = {
  pg: {
    connection: getEnvironmentVariable('PG_APP_CONN_STRING'),
    client: 'pg'
  },
  cookieSessionMaxAge: {
    forUsers: 300000
  },
  api: {
    internalPort: getEnvironmentVariable('INTERNAL_PORT')
  }
}
