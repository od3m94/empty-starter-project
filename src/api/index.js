require('dotenv').config({ path: `${process.cwd()}/etc/.env` })

const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const cookieSession = require('cookie-session')

const config = require('../../infra/config')
const pg = require('../../infra/pg')
const log = require('../shared/log-helper')

const app = express()

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cookieSession({
  name: 'session',
  secret: 'empty-api'
}))

app.use((req, res, next) => {
  req.session.dateNow = (new Date()).getTime()
  req.sessionOptions.maxAge = config.cookieSessionMaxAge.forUsers
  next()
})

// write code here

app.get('/users', async (req, res) => {
  const data = await pg('users as u')
    .select(
      'u.*',
      pg.raw(`array_to_json(array_agg(up) filter (where up is not null)) as user_posts`)
    )
    .leftJoin('user_posts as up', 'u.id', 'up.user_id')
    .groupBy('u.id')
  res.send(data)
})

app.post('/users', async (req, res) => {
  const [data] = await pg('users').insert(req.body).returning('*')
  res.send(data)
})

app.put('/users/:id', async (req, res) => {
  const [data] = await pg('users').update(req.body).where('id', req.params.id).returning('*')
  res.send(data)
})

app.delete('/users/:id', async (req, res) => {
  await pg('users').delete().where('id', req.params.id)
  res.sendStatus(200)
})

app.get('/user-posts', async (req, res) => {
  const data = await pg('user_posts').select('*')
  res.send(data)
})

app.post('/user-posts', async (req, res) => {
  const [data] = await pg('user_posts').insert(req.body).returning('*')
  res.send(data)
})

app.put('/user-posts/:id', async (req, res) => {
  const [data] = await pg('user_posts').update(req.body).where('id', req.params.id).returning('*')
  res.send(data)
})

app.delete('/user-posts/:id', async (req, res) => {
  await pg('user_posts').delete().where('id', req.params.id)
  res.sendStatus(200)
})

app.listen(config.api.internalPort)

process
  .on('unhandledRejection', err => {
    log.error(`${err.message} ${err.stack}`)
  })
  .on('uncaughtException', err => {
    log.error(`${err.message} ${err.stack}`)
  })
