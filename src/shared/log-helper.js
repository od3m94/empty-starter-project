const winston = require('winston')

const { getEnvironmentVariable } = require('./misc-helper')

const logger = winston.createLogger({
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.json()
  ),
  transports: [
    new winston.transports.File({ filename: '/var/log/empty/error.log', level: 'error' }),
    new winston.transports.File({ filename: '/var/log/empty/debug.log', level: 'debug' })
  ]
})

if (getEnvironmentVariable('NODE_ENV') !== 'production') {
  logger.add(new winston.transports.Console({
    format: winston.format.combine(
      winston.format.timestamp(),
      winston.format.json()
    )
  }))
}

module.exports = logger
