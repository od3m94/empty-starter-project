const getEnvironmentVariable = key => {
  return process.env[key]
}

module.exports = {
  getEnvironmentVariable
}
