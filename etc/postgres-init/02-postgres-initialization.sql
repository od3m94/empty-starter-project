\connect empty

create table users (
	id serial not null primary key,
	email text
);

create table user_posts (
	id serial not null primary key,
	text text,
  user_id int references users(id) on delete cascade
);

grant all privileges on all tables in schema public to app;
grant all privileges on all sequences in schema public to app;