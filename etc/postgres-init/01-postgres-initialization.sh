#!/bin/bash

set -o errexit

readonly REQUIRED_ENV_VARS=(
	"PG_USER"
	"PG_PASSWORD"
	"PG_DB"
)

main() {
	check_env_vars_set
	init_user_and_db
}

check_env_vars_set() {
	for required_env_var in ${REQUIRED_ENV_VARS[@]}; do
		if [[ -z "${!required_env_var}" ]]; then
			echo "Error:
    Environment variable '$required_env_var' not set.
    Make sure you have the following environment variables set:
      ${REQUIRED_ENV_VARS[@]}
Aborting."
			exit 1
		fi
	done
}

init_user_and_db() {
	psql -v ON_ERROR_STOP=1 --username "postgres" <<-EOSQL
		     CREATE USER $PG_USER WITH PASSWORD '$PG_PASSWORD';
		     CREATE DATABASE $PG_DB;
		     GRANT ALL PRIVILEGES ON DATABASE $PG_DB TO $PG_USER;
	EOSQL
}

main "$@"
