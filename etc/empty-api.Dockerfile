FROM node:12.18.0

WORKDIR /srv

RUN mkdir -p /var/log/empty

RUN npm i nodemon -g

CMD nodemon ./src/api/index.js
