## EMPTY API

### Environment variables
Environment variables are stored in `./etc/.env` and `./etc/.env-init`

Content example of this file can be found in `./etc/.env-example` and `./etc/.env-init-example` accordingly

### Start development (automatically on change)
Use this command (via npm):
```
npm run start
```